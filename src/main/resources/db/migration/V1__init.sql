create table security
(
    id                            uuid      not null,
    ticker                        varchar   not null,
    exchange                      varchar   not null,
    isin                          varchar,
    short_name                    varchar   not null,
    full_name                     varchar,
    short_lat_name                varchar,
    full_lat_name                 varchar,

    face_value                    numeric(21, 7),
    face_value_currency           int,

    lot_size                      int,
    issue_date                    date,
    issue_size                    bigint,

    issue_capitalization          numeric(21, 7),
    issue_capitalization_currency int,

    type_type                     int,
    type_sub                      int,
    type_raw                      varchar,

    sector_number                 int,
    sector_raw                    varchar,

    qualified_investors           boolean,
    full_update                   timestamp not null,
    version                       int,
    site                          varchar
) with (fillfactor = 80);

create unique index ticker_exchange_uindex on security (exchange, ticker);

create table payment
(
    security_id         uuid           not null,
    date                date           not null,
    type                varchar        not null,
    money               numeric(21, 7) not null,
    money_currency      int            not null,
    value_percent       numeric(21, 7),
    value_rub           numeric(21, 7),

    face_value          numeric(21, 7),
    face_value_currency int
);

create table currency_info
(
    currency_code int,
    currency      varchar
);

create unique index payments_id_date_index on payment (security_id, date DESC);

create table history
(
    security_id         uuid not null,
    date                date not null,
    price               numeric(21, 9),
    max                 numeric(21, 9),
    min                 numeric(21, 9),
    open                numeric(21, 9),
    close               numeric(21, 9),
    num_trades          bigint,
    face_value          numeric(21, 7),
    face_value_currency int,
    currency            int,
    value               numeric(21, 7),
    primary key (security_id, date)
);

create unique index ticker_exchange_index on history (security_id, date DESC);

SELECT public.create_hypertable('history', 'date', 'security_id', 10);

create table currency_pair
(
    id            SERIAL PRIMARY KEY not null,
    currency_from varchar(5)         not null,
    currency_to   varchar(5)         not null,
    unique (currency_from, currency_to)
);

create table currency_history
(
    pair_id int            not null,
    date    date           not null,
    rate    numeric(21, 7) not null,
    primary key (pair_id, date)
);

SELECT public.create_hypertable('currency_history', 'date', 'pair_id', 10);

ALTER TABLE currency_history
    SET (
        timescaledb.compress,
        timescaledb.compress_segmentby = 'pair_id'
        );
SELECT public.add_compression_policy('currency_history', INTERVAL '7 days');


create table scheduled_tasks
(
    task_name            text                     not null,
    task_instance        text                     not null,
    task_data            bytea,
    execution_time       timestamp with time zone not null,
    picked               BOOLEAN                  not null,
    picked_by            text,
    last_success         timestamp with time zone,
    last_failure         timestamp with time zone,
    consecutive_failures INT,
    last_heartbeat       timestamp with time zone,
    version              BIGINT                   not null,
    PRIMARY KEY (task_name, task_instance)
)

