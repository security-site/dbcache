package com.github.klawru.dbcache;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableDiscoveryClient
@EnableConfigurationProperties
@ConfigurationPropertiesScan
@EnableTransactionManagement
@EnableCaching
public class DbcacheApplication {

    public static void main(String[] args) {
        SpringApplication.run(DbcacheApplication.class, args);
    }

}
