package com.github.klawru.dbcache.config;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.security.OAuthFlow;
import io.swagger.v3.oas.models.security.OAuthFlows;
import io.swagger.v3.oas.models.security.SecurityRequirement;
import io.swagger.v3.oas.models.security.SecurityScheme;
import org.javamoney.moneta.Money;
import org.springdoc.core.GroupedOpenApi;
import org.springdoc.core.SpringDocUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.money.MonetaryAmount;

@Configuration
public class WebConfig {

    static {
        SpringDocUtils.getConfig()
                .replaceWithClass(MonetaryAmount.class, org.springdoc.core.converters.models.MonetaryAmount.class)
                .replaceWithClass(Money.class, org.springdoc.core.converters.models.MonetaryAmount.class);
    }

    @Bean
    public OpenAPI customOpenAPI(@Value("${spring.security.oauth2.resourceserver.jwt.issuer-uri}") String jwtUrl) {
        return new OpenAPI()
                .components(new Components()
                        .addSecuritySchemes("OAuth2", new SecurityScheme()
                                .type(SecurityScheme.Type.OAUTH2)
                                .scheme("bearer")
                                .bearerFormat("jwt")
                                .in(SecurityScheme.In.HEADER)
                                .name("Authorization")
                                .flows(new OAuthFlows().implicit(new OAuthFlow()
                                        .authorizationUrl(jwtUrl + "/protocol/openid-connect/auth")
                                ))))
                .addSecurityItem(new SecurityRequirement().addList("OAuth2"))
                .info(new Info().title("DBCache API").version("0.1"));
    }

    @Bean
    public GroupedOpenApi storeOpenApi() {
        String[] paths = {"/**"};
        return GroupedOpenApi.builder().group("DBCache").pathsToMatch(paths)
                .build();
    }
}
