package com.github.klawru.dbcache.config;

import org.springframework.boot.rsocket.messaging.RSocketStrategiesCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.ReactiveAuthenticationManager;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.rsocket.EnableRSocketSecurity;
import org.springframework.security.config.annotation.rsocket.RSocketSecurity;
import org.springframework.security.rsocket.core.PayloadSocketAcceptorInterceptor;
import org.springframework.security.rsocket.metadata.BearerTokenAuthenticationEncoder;
import org.springframework.util.MimeType;

import static com.github.klawru.lib.contract.porfolio.v1.PortfolioInfo.*;
import static com.github.klawru.lib.contract.porfolio.v1.PortfolioInfo.MIME_BANK_IMPORT;
import static io.rsocket.metadata.WellKnownMimeType.MESSAGE_RSOCKET_AUTHENTICATION;

@Configuration
@EnableRSocketSecurity
public class RSocketSecurityConfiguration {

    public static final MimeType AUTHENTICATION_MIME_TYPE = MimeType.valueOf(MESSAGE_RSOCKET_AUTHENTICATION.getString());

    @Bean
    RSocketStrategiesCustomizer bearerTokenAuthenticationEncoder() {
        return strategies -> strategies.encoder(new BearerTokenAuthenticationEncoder());
    }

    @Bean
    PayloadSocketAcceptorInterceptor rsocketSecurity(
            RSocketSecurity rsocket, ReactiveAuthenticationManager manager) {
        rsocket
                .authenticationManager(manager)
                .authorizePayload(authorize ->
                        authorize
                                .anyRequest().authenticated()
                                .anyExchange().permitAll()
                )
                .jwt(Customizer.withDefaults());
        return rsocket.build();
    }


}
