package com.github.klawru.dbcache.config;

import io.rsocket.loadbalance.LoadbalanceTarget;
import io.rsocket.loadbalance.WeightedLoadbalanceStrategy;
import io.rsocket.transport.netty.client.TcpClientTransport;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.ReactiveDiscoveryClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.rsocket.RSocketRequester;
import reactor.core.publisher.Flux;

import java.time.Duration;
import java.util.List;

import static com.github.klawru.lib.contract.config.ClientDiscovery.EXCHANGE_CLIENTS;

@Configuration
public class RSocketClients {

    @Bean
    @Qualifier(EXCHANGE_CLIENTS)
    public RSocketRequester createExchangeClientsRequester(RSocketRequester.Builder builder, ReactiveDiscoveryClient discoveryClient) {
        return builder.transports(targets(EXCHANGE_CLIENTS, discoveryClient), WeightedLoadbalanceStrategy.create());
    }

    public Flux<List<LoadbalanceTarget>> targets(String serviceName, ReactiveDiscoveryClient discoveryClient) {
        return discoveryClient.getInstances(serviceName)
                .map(this::toLoadBalanceTarget)
                .collectList()
                .repeatWhen(longFlux -> longFlux.delayElements(Duration.ofSeconds(5)));
    }

    private LoadbalanceTarget toLoadBalanceTarget(ServiceInstance server) {
        return LoadbalanceTarget.from(
                server.getHost() + server.getPort(),
                TcpClientTransport.create(server.getHost(), server.getPort() + 1));
    }

}
