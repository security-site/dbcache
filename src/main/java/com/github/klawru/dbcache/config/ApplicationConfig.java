package com.github.klawru.dbcache.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

@Data
@ConfigurationProperties(prefix = "application")
public class ApplicationConfig {
    LokiConfig loki;

    public static final class LokiConfig {
        String host;
        Integer port;
    }
}
