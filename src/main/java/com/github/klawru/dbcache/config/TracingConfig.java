package com.github.klawru.dbcache.config;

import brave.handler.MutableSpan;
import brave.handler.SpanHandler;
import brave.propagation.TraceContext;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TracingConfig {

    /**
     * Для работы с трассировкой запросов в Grafana
     */
    @Bean
    SpanHandler appNameSpanTagProvider(@Value("${spring.application.name}") String appName) {
        return new SpanHandler() {
            @Override
            public boolean end(TraceContext context, MutableSpan span, Cause cause) {
                span.tag("app", appName);
                return true;
            }
        };
    }

}
