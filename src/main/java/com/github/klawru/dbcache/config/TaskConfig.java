package com.github.klawru.dbcache.config;

import com.github.kagkarlsson.scheduler.task.Task;
import com.github.kagkarlsson.scheduler.task.helper.Tasks;
import com.github.kagkarlsson.scheduler.task.schedule.Schedules;
import com.github.klawru.dbcache.service.job.SecurityUpdateService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.LocalTime;
import java.time.ZoneId;

@Configuration
public class TaskConfig {

    @Bean
    Task<Void> portfolioCalcTask(SecurityUpdateService service) {
        return Tasks.recurring("portfolioCalc",
                        Schedules.daily(ZoneId.of("Europe/Moscow"), LocalTime.of(1, 0)))
                .execute(((taskInstance, executionContext) -> service.updateAllSecurity(executionContext)));
    }
}
