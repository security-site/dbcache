package com.github.klawru.dbcache.exception;

public class DbCacheException extends RuntimeException {
    public DbCacheException(String message) {
        super(message);
    }

    public DbCacheException(String message, Throwable cause) {
        super(message, cause);
    }
}
