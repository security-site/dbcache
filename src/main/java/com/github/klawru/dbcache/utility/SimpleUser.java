package com.github.klawru.dbcache.utility;

import lombok.Value;

import java.util.UUID;

@Value
public class SimpleUser {
    String username;
    UUID id;
}
