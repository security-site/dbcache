package com.github.klawru.dbcache.utility;

import com.github.f4b6a3.uuid.factory.rfc4122.TimeOrderedFactory;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class UuidGenerator {

    static final TimeOrderedFactory FACTORY = TimeOrderedFactory.builder().build();

    public UUID generate() {
        return FACTORY.create();
    }
}
