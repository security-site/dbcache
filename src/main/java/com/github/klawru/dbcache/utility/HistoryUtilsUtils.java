package com.github.klawru.dbcache.utility;

import lombok.experimental.UtilityClass;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.LocalDate;
import java.util.List;
import java.util.function.Function;

@UtilityClass
public class HistoryUtilsUtils {

    /**
     * @param from    с какого момента оставить историю
     * @param history коллекция должна быть отсортирована по дате в обратном порядке!
     * @param getDate метод для получения даты из элемента коллекции
     * @param <T>     любой тип
     * @return элементы с момета from
     */
    public static <T> List<T> trimHistoryListFrom(@Nullable LocalDate from, @Nullable List<T> history, @Nonnull Function<T, LocalDate> getDate) {
        if (from != null && history != null)
            for (int i = 0; i < history.size(); i++) {
                LocalDate date = getDate.apply(history.get(i));
                if (from.isAfter(date)) {
                    return history.subList(0, i);
                }
            }
        return history;
    }
}
