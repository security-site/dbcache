package com.github.klawru.dbcache.service.cache;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.klawru.dbcache.service.client.ExchangeClientsService;
import com.github.klawru.lib.contract.security.v1.types.request.CurrencyRateRequestDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.currency.CurrencyRateDTO;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.cache.interceptor.SimpleKey;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Signal;

import javax.annotation.Nonnull;
import java.lang.reflect.Method;
import java.text.Collator;
import java.time.Duration;
import java.util.Locale;

@Slf4j
@Service
@CacheConfig(cacheNames = "currency", keyGenerator = "currencyPriceCacheService")
public class CurrencyPriceCacheService implements CacheableService, KeyGenerator {
    private final ExchangeClientsService clientsService;
    private final Cache<Key, ? super Signal<? extends CurrencyRateDTO>> cache;


    public CurrencyPriceCacheService(ExchangeClientsService clientsService) {
        this.clientsService = clientsService;
        cache = Caffeine.newBuilder()
                .expireAfterWrite(Duration.ofMinutes(5))
                .removalListener((key, value, cause) -> log.info("Remove cache({}) ... {}", key, cause))
                .build();
    }

    @Cacheable
    public Mono<CurrencyRateDTO> getCurrencyLastPrice(CurrencyRateRequestDTO requestDTO) {
        return clientsService.getCurrencyRate(requestDTO.withFrom(null))
                .as(this::cacheable);
    }

    @Nonnull
    @Override
    public Object generate(@Nonnull Object target, @Nonnull Method method, Object... params) {
        if (params.length == 0) {
            return SimpleKey.EMPTY;
        }
        if (params.length == 1) {
            Object param = params[0];
            if (param instanceof CurrencyRateRequestDTO requestDTO) {
                return Key.of(requestDTO);
            }
            if (param != null && !param.getClass().isArray()) {
                return param;
            }
        }
        return new SimpleKey(params);
    }

    @Value
    protected static class Key {
        static final Collator usCollator = Collator.getInstance(Locale.US);
        String from;
        String to;

        public Key(String from, String to) {
            if (usCollator.compare(from, to) >= 0) {
                this.from = from;
                this.to = to;
            } else {
                this.from = to;
                this.to = from;
            }
        }

        static Key of(CurrencyRateRequestDTO requestDTO) {
            return new Key(requestDTO.getCurrency().getFrom(), requestDTO.getCurrency().getTo());
        }

        static Key of(String from, String to) {
            return new Key(from, to);
        }

    }

}
