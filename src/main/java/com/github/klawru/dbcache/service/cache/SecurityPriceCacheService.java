package com.github.klawru.dbcache.service.cache;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import com.github.klawru.dbcache.service.client.ExchangeClientsService;
import com.github.klawru.lib.contract.security.v1.types.request.SecurityRequestDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.SecurityDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.SecurityPriceDTO;
import lombok.Value;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.cache.CacheMono;
import reactor.core.publisher.Mono;
import reactor.core.publisher.Signal;

import java.time.Duration;

@Slf4j
@Service
public class SecurityPriceCacheService implements CacheableService {
    private final ExchangeClientsService clientsService;
    private final Cache<Key, ? super Signal<? extends SecurityPriceDTO>> cache;


    public SecurityPriceCacheService(ExchangeClientsService clientsService) {
        this.clientsService = clientsService;
        cache = Caffeine.newBuilder()
                .maximumSize(1000)
                .expireAfterWrite(Duration.ofMinutes(5))
                .removalListener((key, value, cause) -> log.info("Remove cache({}) ... {}", key, cause))
                .build();
    }

    public Mono<SecurityPriceDTO> loadLastInfo(SecurityRequestDTO requestDTO) {
        if (requestDTO.getExchange() == null)
            return Mono.empty();
        return CacheMono.lookup(this.cache.asMap(), Key.of(requestDTO))
                .onCacheMissResume(
                        clientsService.getSecurityPrice(requestDTO)
                                .onErrorResume(throwable -> {
                                    log.error("Error on get SecurityLastInfo from exchangeClients: request:{}", requestDTO, throwable);
                                    return Mono.empty();
                                })
                );
    }

    @Value
    static class Key {
        String ticker;
        String exchange;

        static Key of(SecurityRequestDTO requestDTO) {
            return new Key(requestDTO.getTicker(), requestDTO.getExchange());
        }

        static Key of(String ticker, String exchange) {
            return new Key(ticker, exchange);
        }
    }

}
