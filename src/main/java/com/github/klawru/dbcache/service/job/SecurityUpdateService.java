package com.github.klawru.dbcache.service.job;

import com.github.kagkarlsson.scheduler.task.ExecutionContext;
import com.github.kagkarlsson.scheduler.task.TaskInstance;
import com.github.kagkarlsson.scheduler.task.helper.OneTimeTask;
import com.github.kagkarlsson.scheduler.task.helper.Tasks;
import com.github.klawru.dbcache.dao.SecurityDBService;
import com.github.klawru.dbcache.dao.types.projection.SecurityIdVersion;
import com.github.klawru.dbcache.service.client.ExchangeClientsService;
import com.github.klawru.lib.contract.security.v1.types.request.SecurityRequestDTO;
import com.github.klawru.lib.contract.security.v1.types.request.SecurityRequestType;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.util.retry.Retry;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDate;
import java.util.UUID;

@Slf4j
@Service
@RequiredArgsConstructor
public class SecurityUpdateService {
    private final SecurityDBService service;
    private final ExchangeClientsService exchangeClientsService;
    private final OneTimeTask<UUID> calkPortfolioFor = Tasks.oneTime("calkPortfolioFor", UUID.class)
            .execute(this::updateSecurity);

    public void updateAllSecurity(ExecutionContext executionContext) {
        final var now = Instant.now();
        service.findAllId()
                .map(SecurityIdVersion::getId)
                .subscribe(uuid -> scheduleUpdateSecurityById(executionContext, now, uuid));
    }

    private void scheduleUpdateSecurityById(ExecutionContext executionContext, Instant now, UUID uuid) {
        try {
            executionContext.getSchedulerClient()
                    .schedule(calkPortfolioFor.instance(uuid.toString(), uuid), now);
        } catch (Exception e) {
            log.error("error on schedule calkPortfolioFor='{}'", uuid);
        }
    }

    public void updateSecurity(TaskInstance<UUID> taskInstance, ExecutionContext executionContext) {
        this.service.findWithLastHistory(taskInstance.getData())
                .doOnNext(securityMainInfo -> log.info("{} updateSecurity job start", securityMainInfo))
                .filter(securityMainInfo -> securityMainInfo.last().isBefore(LocalDate.now().minusDays(1)))
                .map(info -> new SecurityRequestDTO(info.ticker(), info.exchange(), info.last(), SecurityRequestType.ALL))
                .flatMapMany(requestDTO -> exchangeClientsService.getSecurity(requestDTO)
                        .flatMap(service::saveSecurityAndReturn)
                )
                .retryWhen(Retry.backoff(2, Duration.ofSeconds(30)))
                .subscribe(
                        dto -> log.info("{} updateSecurity job end", dto),
                        throwable -> log.error("{} updateSecurity job error", taskInstance.getData(), throwable)
                );
    }

}
