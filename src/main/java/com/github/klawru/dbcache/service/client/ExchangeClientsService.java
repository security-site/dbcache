package com.github.klawru.dbcache.service.client;

import com.github.klawru.lib.contract.security.v1.SecurityInfo;
import com.github.klawru.lib.contract.security.v1.types.request.CurrencyRateRequestDTO;
import com.github.klawru.lib.contract.security.v1.types.request.SecurityRequestDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.ExchangeTypeDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.SecurityDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.SecurityPriceDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.currency.CurrencyRateDTO;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.messaging.rsocket.RSocketRequester;
import org.springframework.security.rsocket.metadata.BearerTokenMetadata;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static com.github.klawru.dbcache.config.RSocketSecurityConfiguration.AUTHENTICATION_MIME_TYPE;
import static com.github.klawru.dbcache.utility.SecurityUtils.getCurrentUserJWT;
import static com.github.klawru.lib.contract.config.ClientDiscovery.EXCHANGE_CLIENTS;

@Service
public class ExchangeClientsService implements SecurityInfo {

    private final RSocketRequester requester;
    private static final String MAIN_PATH = EXCHANGE_CLIENTS + "." + SECURITY_INFO + ".";

    public ExchangeClientsService(@Qualifier(EXCHANGE_CLIENTS) RSocketRequester requester) {
        this.requester = requester;
    }

    @Override
    public Flux<SecurityDTO> getSecurity(SecurityRequestDTO request) {
        return getCurrentUserJWT().flatMapMany(jwt ->
                requester.route(MAIN_PATH + GET_SECURITY)
                        .metadata(new BearerTokenMetadata(jwt), AUTHENTICATION_MIME_TYPE)
                        .data(request)
                        .retrieveFlux(SecurityDTO.class));
    }

    @Override
    public Mono<SecurityPriceDTO> getSecurityPrice(SecurityRequestDTO request) {
        return getCurrentUserJWT().flatMap(jwt ->
                requester.route(MAIN_PATH + GET_SECURITY_PRICE)
                        .metadata(new BearerTokenMetadata(jwt), AUTHENTICATION_MIME_TYPE)
                        .data(request)
                        .retrieveMono(SecurityPriceDTO.class));
    }

    @Override
    public Mono<CurrencyRateDTO> getCurrencyRate(CurrencyRateRequestDTO request) {
        return getCurrentUserJWT().flatMap(jwt ->
                requester.route(MAIN_PATH + GET_CURRENCY_RATE)
                        .metadata(new BearerTokenMetadata(jwt), AUTHENTICATION_MIME_TYPE)
                        .data(request)
                        .retrieveMono(CurrencyRateDTO.class));
    }

    @Override
    public Flux<ExchangeTypeDTO> getExchangeType() {
        return getCurrentUserJWT().flatMapMany(jwt ->
                requester.route(MAIN_PATH + GET_EXCHANGE_TYPE)
                        .metadata(new BearerTokenMetadata(jwt), AUTHENTICATION_MIME_TYPE)
                        .retrieveFlux(ExchangeTypeDTO.class));
    }


}
