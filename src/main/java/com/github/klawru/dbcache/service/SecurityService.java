package com.github.klawru.dbcache.service;

import com.github.klawru.dbcache.dao.SecurityDBService;
import com.github.klawru.dbcache.service.cache.SecurityPriceCacheService;
import com.github.klawru.dbcache.service.client.ExchangeClientsService;
import com.github.klawru.lib.contract.security.v1.SecurityInfo;
import com.github.klawru.lib.contract.security.v1.types.request.CurrencyRateRequestDTO;
import com.github.klawru.lib.contract.security.v1.types.request.SecurityRequestDTO;
import com.github.klawru.lib.contract.security.v1.types.request.SecurityRequestType;
import com.github.klawru.lib.contract.security.v1.types.responce.ExchangeTypeDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.HistoryDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.SecurityDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.SecurityPriceDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.currency.CurrencyRateDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static com.github.klawru.dbcache.utility.HistoryUtilsUtils.trimHistoryListFrom;

@Slf4j
@Service
@RequiredArgsConstructor
public class SecurityService implements SecurityInfo {
    private final SecurityDBService dbService;
    private final CurrencyService currencyHistoryService;
    private final SecurityPriceCacheService lastPriceService;
    private final ExchangeClientsService exchangeClientsService;

    public Flux<SecurityDTO> getSecurity(SecurityRequestDTO request) {
        return dbService.loadSecurity(request.getTicker(), request.getExchange(), request.getFrom())
                .flux()
                //Если в базе отсутсвует ценная бумага запрашиваем из ExchangeClients
                .switchIfEmpty(requestFromExchange(request));
    }

    private Flux<SecurityDTO> requestFromExchange(SecurityRequestDTO request) {
        return exchangeClientsService.getSecurity(request.withFrom(null, SecurityRequestType.ALL))
                .flatMap(dbService::saveSecurityAndReturn)
                .doOnNext(securityDTO -> log.debug("mapped securityDTO:{}", securityDTO))
                .map(securityDTO -> trimHistory(securityDTO, request.getFrom()));
    }

    private SecurityDTO trimHistory(SecurityDTO securityDTO, LocalDate from) {
        securityDTO.setHistory(trimHistoryListFrom(from, securityDTO.getHistory(), HistoryDTO::getDate));
        return securityDTO;
    }

    public Mono<SecurityPriceDTO> getSecurityPrice(SecurityRequestDTO request) {
        return lastPriceService.loadLastInfo(request);
    }

    @Override
    public Mono<CurrencyRateDTO> getCurrencyRate(CurrencyRateRequestDTO request) {
        return currencyHistoryService.getCurrencyRate(request);
    }

    @Override
    public Flux<ExchangeTypeDTO> getExchangeType() {
        return exchangeClientsService.getExchangeType();
    }


    private <T> Optional<T> getFirst(List<T> list) {
        if (!list.isEmpty()) {
            return Optional.ofNullable(list.get(0));
        }
        return Optional.empty();
    }
}
