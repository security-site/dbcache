package com.github.klawru.dbcache.service.cache;

import reactor.core.publisher.Mono;

import java.time.Duration;

import static java.time.temporal.ChronoUnit.MINUTES;

public interface CacheableService {

    default <T> Mono<T> cacheable(Mono<T> mono) {
        return mono.cache(
                result -> Duration.of(5, MINUTES),
                throwable -> Duration.ZERO,
                () -> Duration.ZERO);
    }
}
