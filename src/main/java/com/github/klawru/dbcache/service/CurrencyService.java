package com.github.klawru.dbcache.service;

import com.github.klawru.dbcache.dao.CurrencyDBService;
import com.github.klawru.dbcache.dao.mappers.CurrencyRateMapper;
import com.github.klawru.dbcache.service.cache.CurrencyPriceCacheService;
import com.github.klawru.dbcache.service.client.ExchangeClientsService;
import com.github.klawru.lib.contract.security.v1.types.request.CurrencyRateRequestDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.currency.CurrencyRateDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.currency.RateHistoryDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import javax.annotation.Nonnull;

import static com.github.klawru.dbcache.utility.HistoryUtilsUtils.trimHistoryListFrom;
import static com.github.klawru.lib.contract.security.v1.SecurityInfo.FROM_MIN_DATE;

@Slf4j
@Service
@RequiredArgsConstructor
public class CurrencyService {
    private final CurrencyDBService currencyDBService;
    private final ExchangeClientsService exchangeClientsService;
    private final CurrencyPriceCacheService lastPriceService;
    private static final CurrencyRateMapper currencyMapper = CurrencyRateMapper.INSTANCE;

    public Mono<CurrencyRateDTO> getCurrencyRate(CurrencyRateRequestDTO request) {
        return currencyDBService.getCurrencyRate(request)
                .zipWith(lastPriceService.getCurrencyLastPrice(request), Pair::of)
                .map(tuple -> currencyMapper.map(tuple.getLeft().getKey(), tuple.getRight().getLast(), tuple.getLeft().getRight()))
                .switchIfEmpty(Mono.defer(() -> exchangeClientsService.getCurrencyRate(request.withFrom(FROM_MIN_DATE))
                        .flatMap(currencyDBService::saveCurrencyRateAndReturn)
                        .map(dto -> trimHistory(request, dto)))
                );
    }

    @Nonnull
    private CurrencyRateDTO trimHistory(CurrencyRateRequestDTO request, CurrencyRateDTO dto) {
        final var list = trimHistoryListFrom(request.getFrom(),
                dto.getHistory(),
                RateHistoryDTO::getDate);
        return dto.withHistory(list);
    }

}
