package com.github.klawru.dbcache.controller;

import com.github.klawru.dbcache.exception.DbCacheException;
import com.github.klawru.dbcache.service.SecurityService;
import com.github.klawru.lib.contract.security.v1.SecurityInfo;
import com.github.klawru.lib.contract.security.v1.types.request.CurrencyRateRequestDTO;
import com.github.klawru.lib.contract.security.v1.types.request.SecurityRequestDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.ExchangeTypeDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.SecurityDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.SecurityPriceDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.currency.CurrencyRateDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.Valid;

import static com.github.klawru.lib.contract.security.v1.SecurityInfo.SECURITY_INFO;


@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping(value = "dbcache/" + SECURITY_INFO, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
public class MainWebController implements SecurityInfo {

    private final SecurityService service;

    @Operation(description = "Получить информацию по бумаге")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Получить информацию по бумаге")})
    @PostMapping(value = GET_SECURITY)
    public Flux<SecurityDTO> getSecurity(@RequestBody SecurityRequestDTO request) {
        return service.getSecurity(request);
    }

    @Operation(description = "Получить последнюю цену по бумаге по бумаге. (Указание идинтификатора биржы обязательно (Московская: MISX) )")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Информация по последней цене бумаге")})
    @PostMapping(value = GET_SECURITY_PRICE)
    public Mono<SecurityPriceDTO> getSecurityPrice(@Valid @RequestBody SecurityRequestDTO request) {
        if (StringUtils.isBlank(request.getExchange()))
            return Mono.error(() -> new DbCacheException("request.exchange should not empty"));
        return service.getSecurityPrice(request);
    }

    @Override
    @Operation(description = "Получить коэфиценты для валютной пары.")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Информация по последней цене бумаге")})
    @PostMapping(value = GET_CURRENCY_RATE)
    public Mono<CurrencyRateDTO> getCurrencyRate(@Valid @RequestBody CurrencyRateRequestDTO request) {
        return service.getCurrencyRate(request);
    }

    @Override
    @Operation(description = "Получить список всех поддерживаемых бирж")
    @ApiResponses(value = {@ApiResponse(responseCode = "200", description = "Поддерживаемые биржи")})
    @GetMapping(value = GET_EXCHANGE_TYPE)
    public Flux<ExchangeTypeDTO> getExchangeType() {
        return service.getExchangeType();
    }

}
