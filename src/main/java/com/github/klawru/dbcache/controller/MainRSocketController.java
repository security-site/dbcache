package com.github.klawru.dbcache.controller;

import com.github.klawru.dbcache.service.SecurityService;
import com.github.klawru.lib.contract.security.v1.SecurityInfo;
import com.github.klawru.lib.contract.security.v1.types.request.CurrencyRateRequestDTO;
import com.github.klawru.lib.contract.security.v1.types.request.SecurityRequestDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.ExchangeTypeDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.SecurityDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.SecurityPriceDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.currency.CurrencyRateDTO;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.stereotype.Controller;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static com.github.klawru.lib.contract.config.ClientDiscovery.DBCACHE;
import static com.github.klawru.lib.contract.security.v1.SecurityInfo.SECURITY_INFO;


@Slf4j
@Controller()
@RequiredArgsConstructor
@MessageMapping(DBCACHE + "." + SECURITY_INFO + ".")
public class MainRSocketController implements SecurityInfo {

    private final SecurityService service;

    @Override
    @MessageMapping(GET_SECURITY)
    public Flux<SecurityDTO> getSecurity(SecurityRequestDTO request) {
        return service.getSecurity(request);
    }

    @Override
    @MessageMapping(GET_SECURITY_PRICE)
    public Mono<SecurityPriceDTO> getSecurityPrice(SecurityRequestDTO request) {
        return service.getSecurityPrice(request);
    }

    @Override
    @MessageMapping(GET_CURRENCY_RATE)
    public Mono<CurrencyRateDTO> getCurrencyRate(CurrencyRateRequestDTO request) {
        return service.getCurrencyRate(request);
    }

    @Override
    @MessageMapping(GET_EXCHANGE_TYPE)
    public Flux<ExchangeTypeDTO> getExchangeType() {
        return service.getExchangeType();
    }

}
