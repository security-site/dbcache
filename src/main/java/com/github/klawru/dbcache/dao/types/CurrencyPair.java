package com.github.klawru.dbcache.dao.types;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.With;
import org.springframework.data.annotation.Id;
import org.springframework.data.domain.Persistable;
import org.springframework.data.relational.core.mapping.Column;

@Data
@AllArgsConstructor
public class CurrencyPair implements Persistable<Integer> {
    @Id
    @With
    Integer id;
    @Column("currency_from")
    String from;
    @Column("currency_to")
    String to;

    @Override
    public boolean isNew() {
        return id == null;
    }
}
