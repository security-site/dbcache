package com.github.klawru.dbcache.dao.mappers;

import com.github.klawru.dbcache.dao.types.History;
import com.github.klawru.lib.contract.security.v1.types.responce.HistoryDTO;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(uses = MoneyMapper.class)
public abstract class HistoryMapper {
    public static final HistoryMapper INSTANCE = Mappers.getMapper(HistoryMapper.class);

    @Mapping(target = "withSecurityId", ignore = true)
    @Mapping(target = "securityId", ignore = true)
    @Mapping(target = "price", source = "price")
    @Mapping(target = "max", source = "max")
    @Mapping(target = "min", source = "min")
    @Mapping(target = "open", source = "open")
    @Mapping(target = "close", source = "close")
    @Mapping(target = "currency", source = "currency", qualifiedByName = {"MoneyMapper", "currency2Integer"})
    @Mapping(target = "faceValue", source = "faceValue.amount")
    @Mapping(target = "faceValueCurrency", source = "faceValue.currency", qualifiedByName = {"MoneyMapper", "currency2Integer"})
    abstract History dtoMapToEntity(HistoryDTO dto);

    @InheritInverseConfiguration
    abstract HistoryDTO entityMapToDto(History dto);

}
