package com.github.klawru.dbcache.dao.types.projection;

import lombok.Value;

@Value
public class OnlyId {
    Integer id;
}
