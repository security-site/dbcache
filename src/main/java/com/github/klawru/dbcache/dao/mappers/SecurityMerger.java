package com.github.klawru.dbcache.dao.mappers;

import com.github.klawru.dbcache.dao.types.Security;
import org.mapstruct.*;

@Mapper(nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS, nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface SecurityMerger {

    @Mapping(target = "withId", ignore = true)
    @Mapping(target = "history", ignore = true)
    @Mapping(target = "payments", ignore = true)
    Security to(@MappingTarget Security target, Security newSecurity);

}
