package com.github.klawru.dbcache.dao.types;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.data.relational.core.mapping.Column;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
@AllArgsConstructor
public class CurrencyHistory {
    @Column("pair_id")
    Integer pairId;
    LocalDate date;
    BigDecimal rate;
}
