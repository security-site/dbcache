package com.github.klawru.dbcache.dao.types;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.With;
import org.springframework.data.annotation.Id;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

@Data
@AllArgsConstructor
public class History {
    @Id
    @With
    UUID securityId;
    LocalDate date;

    /**
     * Средневзвешенная цена
     */
    BigDecimal price;
    BigDecimal max;
    BigDecimal min;
    BigDecimal open;
    BigDecimal close;
    Integer currency;

    BigDecimal faceValue;
    Integer faceValueCurrency;

    /**
     * Объем сделок за день
     */
    BigDecimal value;
    /**
     * Количество сделок за день, штук
     */
    Long numTrades;
}
