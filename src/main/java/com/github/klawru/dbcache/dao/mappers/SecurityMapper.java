package com.github.klawru.dbcache.dao.mappers;

import com.github.klawru.dbcache.dao.types.Security;
import com.github.klawru.lib.contract.security.v1.types.responce.*;
import org.javamoney.moneta.Money;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import javax.money.NumberValue;
import java.math.BigDecimal;

@Mapper(uses = {PaymentsMapper.class, HistoryMapper.class, MoneyMapper.class})
public abstract class SecurityMapper {
    public static final SecurityMapper INSTANCE = Mappers.getMapper(SecurityMapper.class);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "withId", ignore = true)
    @Mapping(target = "version", ignore = true)
    @Mapping(target = "faceValue", source = "faceValue.amount")
    @Mapping(target = "faceValueCurrency", source = "faceValue.currency", qualifiedByName = {"MoneyMapper", "currency2Integer"})
    @Mapping(target = "payments", source = "payments", qualifiedByName = {"PaymentsMapper", "entity2Dto"})
    @Mapping(target = "issueCapitalization", source = "issueCapitalization.number")
    @Mapping(target = "issueCapitalizationCurrency", source = "issueCapitalization.currency.currencyCode", qualifiedByName = {"MoneyMapper", "currency2Integer"})
    @Mapping(target = "typeType", source = "type.type")
    @Mapping(target = "typeSub", source = "type.subType")
    @Mapping(target = "typeRaw", source = "type.raw")
    @Mapping(target = "sectorRaw", source = "sector.raw")
    @Mapping(target = "sectorNumber", source = "sector.type")
    @Mapping(target = "fullUpdate", ignore = true)
    public abstract Security dtoToEntity(SecurityDTO securityDTO);

    @InheritInverseConfiguration
    @Mapping(target = "issueCapitalization", expression = "java(issueCapitalizationMap(security))")
    @Mapping(target = "payments", source = "payments", qualifiedByName = {"PaymentsMapper", "entity2Dto"})
    @Mapping(target = "lastUpdate", source = "fullUpdate")
    public abstract SecurityDTO entityToDto(Security security);

    Money issueCapitalizationMap(Security security) {
        final var issueCapitalization = security.getIssueCapitalization();
        final var currencyId = security.getIssueCapitalizationCurrency();
        if (issueCapitalization == null || currencyId == null)
            return null;
        return Money.of(issueCapitalization, MoneyMapper.currencyToCode(currencyId));
    }

    BigDecimal map(NumberValue value) {
        if (value == null)
            return null;
        return value.numberValue(BigDecimal.class);
    }

    Integer securityType2Int(SecurityType value) {
        if (value == null)
            return null;
        return value.ordinal();
    }

    Integer subType2int(SecuritySubType value) {
        if (value == null)
            return null;
        return value.ordinal();
    }

    SecurityType int2SecurityType(Integer value) {
        if (value == null)
            return SecurityType.UNRECOGNIZED;
        final var values = SecurityType.values();
        if (value > 0 && value < values.length)
            return values[value];
        else
            return SecurityType.UNRECOGNIZED;
    }

    Integer sectorType2Int(SectorType value) {
        if (value == null)
            return null;
        return value.ordinal();
    }

    SectorType int2SectorType(Integer value) {
        if (value == null)
            return SectorType.UNRECOGNIZED;
        final var values = SectorType.values();
        if (value > 0 && value < values.length)
            return values[value];
        else
            return SectorType.UNRECOGNIZED;
    }


    SecuritySubType int2SecuritySubType(Integer value) {
        if (value == null)
            return SecuritySubType.UNRECOGNIZED;
        final var values = SecuritySubType.values();
        if (value > 0 && value < values.length)
            return values[value];
        else
            return SecuritySubType.UNRECOGNIZED;
    }

    protected SectorDTO securityToSectorDTO(Security security) {
        if (security == null || security.getSectorRaw() == null) {
            return null;
        }

        String raw = security.getSectorRaw();
        SectorType type = int2SectorType(security.getSectorNumber());

        return new SectorDTO(type, raw);
    }


}
