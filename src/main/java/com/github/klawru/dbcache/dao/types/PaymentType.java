package com.github.klawru.dbcache.dao.types;

public enum PaymentType {
    COUPON,
    DIVIDEND,
    AMORTIZATION
}
