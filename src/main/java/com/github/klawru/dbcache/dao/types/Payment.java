package com.github.klawru.dbcache.dao.types;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.With;
import org.springframework.data.annotation.Id;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;

@Data
@AllArgsConstructor
public class Payment {
    @Id
    @With
    UUID securityId;
    PaymentType type;

    LocalDate date;

    BigDecimal money;
    Integer moneyCurrency;

    BigDecimal valuePercent;
    BigDecimal valueRub;

    BigDecimal faceValue;
    Integer faceValueCurrency;
}
