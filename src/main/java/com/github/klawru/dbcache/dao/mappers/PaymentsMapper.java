package com.github.klawru.dbcache.dao.mappers;

import com.github.klawru.dbcache.dao.types.Payment;
import com.github.klawru.lib.contract.security.v1.types.responce.payment.AbstractPaymentDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.payment.AmortizationDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.payment.CouponDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.payment.DividendDTO;
import org.mapstruct.*;
import org.mapstruct.factory.Mappers;

@Mapper(uses = {MoneyMapper.class})
@Named("PaymentsMapper")
public interface PaymentsMapper {

    PaymentsMapper INSTANCE = Mappers.getMapper(PaymentsMapper.class);

    @Named("entity2Dto")
    default AbstractPaymentDTO entityMapToDto(Payment payment) {
        return switch (payment.getType()) {
            case COUPON -> payment2Coupon(payment);
            case DIVIDEND -> payment2Dividend(payment);
            case AMORTIZATION -> payment2Amortization(payment);
        };
    }

    @Named("entity2Dto")
    default Payment dtoMapToEntity(AbstractPaymentDTO payments) {
        return switch (payments.getClass().getSimpleName()) {
            case "DividendDTO" -> dividend2Payment((DividendDTO) payments);
            case "CouponDTO" -> coupon2Payment((CouponDTO) payments);
            case "AmortizationDTO" -> amortization2Payment((AmortizationDTO) payments);
            default -> throw new IllegalArgumentException("Unknown payment class:" + payments.getClass().getName());
        };
    }


    @Mapping(target = "withSecurityId", ignore = true)
    @Mapping(target = "securityId", ignore = true)
    @Mapping(target = "type", constant = "DIVIDEND")
    @Mapping(target = "money", source = "money.amount")
    @Mapping(target = "moneyCurrency", source = "money.currency", qualifiedByName = {"MoneyMapper", "currency2Integer"})
    @Mapping(target = "valueRub", ignore = true)
    @Mapping(target = "valuePercent", ignore = true)
    @Mapping(target = "faceValue", ignore = true)
    @Mapping(target = "faceValueCurrency", ignore = true)
    Payment dividend2Payment(DividendDTO dividendDTO);

    @Mapping(target = "withSecurityId", ignore = true)
    @Mapping(target = "securityId", ignore = true)
    @Mapping(target = "type", constant = "COUPON")
    @Mapping(target = "money", source = "money.amount")
    @Mapping(target = "moneyCurrency", source = "money.currency", qualifiedByName = {"MoneyMapper", "currency2Integer"})
    @Mapping(target = "valuePercent", source = "valuePercent")
    @Mapping(target = "valueRub", source = "valueRub.amount")
    @Mapping(target = "faceValue", source = "faceValue.amount")
    @Mapping(target = "faceValueCurrency", source = "faceValue.currency", qualifiedByName = {"MoneyMapper", "currency2Integer"})
    Payment coupon2Payment(CouponDTO payments);


    @InheritConfiguration(name = "coupon2Payment")
    @Mapping(target = "withSecurityId", ignore = true)
    @Mapping(target = "securityId", ignore = true)
    @Mapping(target = "type", constant = "AMORTIZATION")
    Payment amortization2Payment(AmortizationDTO payments);


    @InheritInverseConfiguration
    DividendDTO payment2Dividend(Payment payment);

    @InheritInverseConfiguration(name = "amortization2Payment")
    @Mapping(target = "valueRub.currency", constant = "RUB")
    AmortizationDTO payment2Amortization(Payment payment);

    @InheritInverseConfiguration
    @Mapping(target = "valueRub.currency", constant = "RUB")
    CouponDTO payment2Coupon(Payment payment);


}
