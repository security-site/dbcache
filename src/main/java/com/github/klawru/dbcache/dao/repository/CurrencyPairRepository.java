package com.github.klawru.dbcache.dao.repository;

import com.github.klawru.dbcache.dao.types.CurrencyPair;
import org.springframework.data.r2dbc.repository.Modifying;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import reactor.core.publisher.Mono;

public interface CurrencyPairRepository extends R2dbcRepository<CurrencyPair, Integer> {

    @Query("SELECT *  FROM currency_pair " +
            "WHERE (currency_from = :from AND currency_to = :to) " +
            "   OR (currency_to = :from AND currency_from = :to) " +
            "LIMIT 1")
    Mono<CurrencyPair> findTopByFromAndTo(String from, String to);

    default Mono<Integer> saveIgnore(CurrencyPair pair) {
        return saveIgnore(pair.getFrom(), pair.getTo())
                .then(findTopByFromAndTo(pair.getFrom(), pair.getTo()))
                .mapNotNull(CurrencyPair::getId);
    }

    /**
     * @return количество вставленных строк
     */
    @Modifying
    @Query("INSERT INTO currency_pair (id, currency_from, currency_to) " +
            "VALUES (default, :from, :to) ON CONFLICT DO NOTHING")
    Mono<Integer> saveIgnore(String from, String to);
}
