package com.github.klawru.dbcache.dao.repository;

import com.github.klawru.dbcache.dao.types.Security;
import com.github.klawru.dbcache.dao.types.projection.SecurityIdVersion;
import com.github.klawru.dbcache.dao.types.projection.SecurityMainInfo;
import org.springframework.data.r2dbc.repository.Modifying;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.util.UUID;

public interface SecurityRepository extends R2dbcRepository<Security, UUID> {

    Mono<Security> findByTickerAndExchange(String ticker, String exchange);

    @Query("SELECT id, version FROM security where ticker=:ticker AND (:exchange IS NULL OR exchange=:exchange)")
    Mono<SecurityIdVersion> findIdByTickerAndExchange(String ticker, String exchange);

    Flux<Security> findByTicker(String ticker);

    @Query("SELECT id, version FROM security")
    Flux<SecurityIdVersion> findAllSecurityId();

    @Query("SELECT s.id, s.ticker, s.exchange, last.date as last " +
            "FROM security s " +
            "         cross join LATERAL (select date " +
            "                             from history h " +
            "                             where h.security_id = s.id " +
            "                             ORDER BY date desc " +
            "                             LIMIT 1) as last " +
            "WHERE s.id = :id")
    Mono<SecurityMainInfo> findByIdMainInfo(UUID id);

    default Mono<SecurityIdVersion> saveIgnore(Security security) {
        return saveIgnore(security.getId(), security.getTicker(), security.getExchange(), security.getIsin(),
                security.getShortName(), security.getFullName(), security.getShortLatName(), security.getFullLatName(),
                security.getFaceValue(), security.getFaceValueCurrency(),
                security.getLotSize(), security.getIssueDate(), security.getIssueSize(),
                security.getIssueCapitalization(), security.getIssueCapitalizationCurrency(),
                security.getTypeType(), security.getTypeSub(), security.getTypeRaw(),
                security.getSectorNumber(), security.getSectorRaw(),
                security.getQualifiedInvestors(), security.getFullUpdate(), security.getVersion(),
                security.getSite())
                .then(findIdByTickerAndExchange(security.getTicker(), security.getExchange()));
    }

    @Modifying
    @Query("""
            INSERT INTO security (id, ticker, exchange, isin, short_name, full_name, short_lat_name, full_lat_name,
                                  face_value, face_value_currency, lot_size, issue_date, issue_size, issue_capitalization,
                                  issue_capitalization_currency, type_type, type_sub, type_raw,
                                  sector_number, sector_raw,
                                  qualified_investors, full_update, version, site)
            VALUES (:id, :ticker, :exchange, :isin, :shortName, :fullName, :shortLatName, :fullLatName,
                    :faceValue, :faceValueCurrency, :lotSize, :issueDate, :issueSize,
                    :issueCapitalization, :issueCapitalizationCurrency, :typeType, :typeSub, :typeRaw,
                    :sectorNumber, :sectorRaw,
                    :qualifiedInvestors, :fullUpdate, :version, :site)
            ON CONFLICT DO NOTHING""")
    Mono<Integer> saveIgnore(UUID id, String ticker, String exchange, String isin,
                             String shortName, String fullName, String shortLatName, String fullLatName,
                             BigDecimal faceValue, Integer faceValueCurrency,
                             Integer lotSize, LocalDate issueDate, Long issueSize,
                             BigDecimal issueCapitalization, Integer issueCapitalizationCurrency,
                             Integer typeType, Integer typeSub, String typeRaw,
                             Integer sectorNumber, String sectorRaw,
                             Boolean qualifiedInvestors, Instant fullUpdate, Integer version, String site);
}
