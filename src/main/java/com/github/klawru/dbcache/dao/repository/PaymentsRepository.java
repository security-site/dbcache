package com.github.klawru.dbcache.dao.repository;

import com.github.klawru.dbcache.dao.types.Payment;
import com.github.klawru.dbcache.dao.types.PaymentType;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.data.r2dbc.repository.Modifying;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.UUID;


public interface PaymentsRepository extends R2dbcRepository<Payment, Pair<UUID, LocalDate>> {

    Flux<Payment> findBySecurityIdAndDateAfterOrderByDateDesc(@NotNull UUID securityId, @NotNull LocalDate date);

    Flux<Payment> findBySecurityIdOrderByDateDesc(@NotNull UUID securityId);

    @Modifying
    default Flux<Integer> saveAllIgnore(Iterable<Payment> entity) {
        return Flux.fromIterable(entity).concatMap(date -> saveIgnore(
                date.getSecurityId(), date.getDate(), date.getType(),
                date.getMoney(), date.getMoneyCurrency(),
                date.getValuePercent(), date.getValueRub(),
                date.getFaceValue(), date.getFaceValueCurrency()
        ));
    }

    @Modifying
    @Query("""
            INSERT INTO payment (security_id, date, type,
                                 money, money_currency, value_percent, value_rub,
                                 face_value, face_value_currency)
            VALUES (:securityId, :date, :type,
                    :money, :moneyCurrency, :valuePercent, :valueRubValue,
                    :faceValue, :faceValueCurrency)
            ON CONFLICT DO NOTHING""")
    Mono<Integer> saveIgnore(UUID securityId, LocalDate date, PaymentType type,
                             BigDecimal money, Integer moneyCurrency,
                             BigDecimal valuePercent, BigDecimal valueRubValue,
                             BigDecimal faceValue, Integer faceValueCurrency);

}
