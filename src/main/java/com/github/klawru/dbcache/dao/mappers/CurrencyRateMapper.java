package com.github.klawru.dbcache.dao.mappers;

import com.github.klawru.dbcache.dao.types.CurrencyHistory;
import com.github.klawru.dbcache.dao.types.CurrencyPair;
import com.github.klawru.lib.contract.security.v1.types.responce.currency.CurrencyPairDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.currency.CurrencyRateDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.currency.RateHistoryDTO;
import org.eclipse.collections.impl.factory.Lists;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import javax.annotation.Nullable;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

@Mapper
public interface CurrencyRateMapper {
    CurrencyRateMapper INSTANCE = Mappers.getMapper(CurrencyRateMapper.class);

    @Mapping(target = "withHistory", ignore = true)
    CurrencyRateDTO map(CurrencyPair currency, BigDecimal last, List<CurrencyHistory> history);

    CurrencyHistory mapToHistoryList(RateHistoryDTO dto, Integer pairId);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "withId", ignore = true)
    default CurrencyPair mapToPair(CurrencyPairDTO dto) {
        final var from = dto.getFrom();
        final var to = dto.getTo();
        return new CurrencyPair(null, from, to);
    }


    default List<CurrencyHistory> mapToHistoryList(List<RateHistoryDTO> history, Integer pairId) {
        Objects.requireNonNull(history);
        Objects.requireNonNull(pairId);
        return Lists.adapt(history).collect(dto -> mapToHistoryList(dto, pairId));
    }
}
