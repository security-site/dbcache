package com.github.klawru.dbcache.dao.types.projection;

import lombok.Value;
import lombok.experimental.Accessors;

import java.time.LocalDate;
import java.util.UUID;

@Value
@Accessors(fluent = true)
public class SecurityMainInfo {
    UUID id;
    String ticker;
    String exchange;
    LocalDate last;
}
