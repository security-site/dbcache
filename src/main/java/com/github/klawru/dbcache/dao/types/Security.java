package com.github.klawru.dbcache.dao.types;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.With;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.annotation.Transient;
import org.springframework.data.annotation.Version;
import org.springframework.data.domain.Persistable;

import javax.annotation.Nullable;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
public class Security implements Persistable<UUID> {
    @Id
    @With
    UUID id;
    @Size(max = 10)
    String ticker;
    @Size(max = 30)
    String exchange;
    @Size(max = 12)
    String isin;
    @Size(max = 30)
    String shortName;
    @Size(max = 90)
    String fullName;
    @Size(max = 30)
    String shortLatName;
    @Size(max = 90)
    String fullLatName;

    BigDecimal faceValue;
    Integer faceValueCurrency;

    Integer lotSize;
    LocalDate issueDate;
    Long issueSize;

    BigDecimal issueCapitalization;
    Integer issueCapitalizationCurrency;

    @Nullable
    Integer typeType;
    @Nullable
    Integer typeSub;
    @Nullable
    String typeRaw;

    Integer sectorNumber;
    String sectorRaw;

    Boolean qualifiedInvestors;

    @Nullable
    String site;

    @Transient
    @Accessors(chain = true)
    List<History> history;
    @Transient
    @Accessors(chain = true)
    List<Payment> payments;

    @Version
    Integer version;
    @LastModifiedDate
    Instant fullUpdate;

    @Override
    public boolean isNew() {
        return version == null;
    }
}
