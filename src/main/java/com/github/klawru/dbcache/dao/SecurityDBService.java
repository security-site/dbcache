package com.github.klawru.dbcache.dao;

import com.github.klawru.dbcache.dao.mappers.SecurityMapper;
import com.github.klawru.dbcache.dao.repository.HistoryDayRepository;
import com.github.klawru.dbcache.dao.repository.PaymentsRepository;
import com.github.klawru.dbcache.dao.repository.SecurityRepository;
import com.github.klawru.dbcache.dao.types.History;
import com.github.klawru.dbcache.dao.types.Payment;
import com.github.klawru.dbcache.dao.types.Security;
import com.github.klawru.dbcache.dao.types.projection.SecurityIdVersion;
import com.github.klawru.dbcache.dao.types.projection.SecurityMainInfo;
import com.github.klawru.dbcache.utility.UuidGenerator;
import com.github.klawru.lib.contract.security.v1.types.responce.SecurityDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import org.springframework.transaction.reactive.TransactionalOperator;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.time.Instant;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.function.Function;

@Component
@RequiredArgsConstructor
public class SecurityDBService {
    private static final SecurityMapper mapper = SecurityMapper.INSTANCE;

    private final SecurityRepository securityRepository;
    private final PaymentsRepository paymentsRepository;
    private final HistoryDayRepository historyDayRepository;
    private final TransactionalOperator transactionalOperator;
    private final UuidGenerator uuidCreator;


    public Mono<SecurityDTO> loadSecurity(String ticker, @Nullable String exchange, @Nullable LocalDate from) {
        return securityRepository.findByTickerAndExchange(ticker, exchange)
                .flatMap(security -> loadHistoryAndPayments(from, security));
    }

    private Mono<SecurityDTO> loadHistoryAndPayments(@Nullable LocalDate from, Security security) {
        return Mono.zip(
                        getHistory(from, security),
                        getPayments(from, security),
                        (s1, s2) -> s1)
                .map(mapper::entityToDto);

    }

    private Mono<Security> getPayments(@Nullable LocalDate from, Security security) {
        if (from == null)
            return paymentsRepository.findBySecurityIdOrderByDateDesc(security.getId())
                    .collectList()
                    .map(security::setPayments);
        return paymentsRepository.findBySecurityIdAndDateAfterOrderByDateDesc(security.getId(), from)
                .collectList()
                .map(security::setPayments);
    }

    private Mono<Security> getHistory(@Nullable LocalDate from, Security security) {
        if (from == null)
            return historyDayRepository.findBySecurityIdOrderByDateDesc(security.getId())
                    .collectList()
                    .map(security::setHistory);
        return historyDayRepository.findBySecurityIdAndDateAfterOrderByDateDesc(security.getId(), from)
                .collectList()
                .map(security::setHistory);
    }

    public Flux<SecurityDTO> saveSecurityAndReturn(SecurityDTO securityDTO) {
        return Mono.just(securityDTO)
                .map(mapper::dtoToEntity)
                .flatMap(this::saveSecurityAndReturn)
                .thenMany(Mono.just(securityDTO));
    }

    protected Mono<Void> saveSecurityAndReturn(Security security) {
        final var historyList = security.getHistory();
        final var paymentList = security.getPayments();
        return save(security, historyList, paymentList)
                .then();
    }

    private Mono<SecurityIdVersion> save(Security security, List<History> historyList, List<Payment> paymentList) {
        return saveSecurity(security)
                .flatMap(id -> Flux.concat(
                                saveEntityList(historyList, history -> history.setSecurityId(id.getId()), historyDayRepository::saveAllIgnore),
                                saveEntityList(paymentList, payment -> payment.setSecurityId(id.getId()), paymentsRepository::saveAllIgnore))
                        .then(Mono.just(id))
                )
                .as(transactionalOperator::transactional);
    }

    protected Mono<SecurityIdVersion> saveSecurity(Security security) {
        return Mono.defer(() -> {
            if (security.getId() == null)
                security.setId(uuidCreator.generate());
            security.setFullUpdate(Instant.now());
            return securityRepository.saveIgnore(security);
        });
    }


    @Nonnull
    private <E> Mono<Void> saveEntityList(List<E> list, Consumer<E> setId, Function<List<E>, Flux<Integer>> saveAll) {
        return Mono.just(Optional.ofNullable(list)
                        .orElse(Collections.emptyList())
                )
                .doOnNext(eList -> eList.forEach(setId))
                .flatMapMany(saveAll)
                .then();
    }

    public Mono<SecurityMainInfo> findWithLastHistory(UUID id) {
        return securityRepository.findByIdMainInfo(id);
    }

    public Flux<SecurityIdVersion> findAllId() {
        return securityRepository.findAllSecurityId();
    }
}
