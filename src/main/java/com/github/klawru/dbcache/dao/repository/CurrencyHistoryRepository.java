package com.github.klawru.dbcache.dao.repository;

import com.github.klawru.dbcache.dao.types.CurrencyHistory;
import org.eclipse.collections.api.tuple.Pair;
import org.springframework.data.r2dbc.repository.Modifying;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.time.LocalDate;

public interface CurrencyHistoryRepository extends R2dbcRepository<CurrencyHistory, Pair<Integer, LocalDate>> {

    Flux<CurrencyHistory> findFirstByPairIdOrderByDateDesc(Integer pairId);

    Flux<CurrencyHistory> findAllByPairIdOrderByDateDesc(Integer pairId);

    Flux<CurrencyHistory> findAllByPairIdAndDateAfterOrderByDateDesc(Integer pairId, LocalDate from);

    @Modifying
    default Flux<Integer> saveAllIgnore(Iterable<CurrencyHistory> entity) {
        return Flux.fromIterable(entity)
                .concatMap(history -> saveIgnore(history.getPairId(), history.getDate(), history.getRate()));
    }

    @Modifying
    @Query("INSERT INTO currency_history (pair_id, date, rate) " +
            "VALUES (:pairId, :date, :rate) ON CONFLICT DO NOTHING")
    Mono<Integer> saveIgnore(Integer pairId, LocalDate date, BigDecimal rate);
}
