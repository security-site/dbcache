package com.github.klawru.dbcache.dao.types.projection;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.With;

import java.io.Serializable;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SecurityIdVersion implements Serializable {
    @With
    UUID id;
    Integer version;
}
