package com.github.klawru.dbcache.dao;

import com.github.klawru.dbcache.dao.mappers.CurrencyRateMapper;
import com.github.klawru.dbcache.dao.repository.CurrencyHistoryRepository;
import com.github.klawru.dbcache.dao.repository.CurrencyPairRepository;
import com.github.klawru.dbcache.dao.types.CurrencyHistory;
import com.github.klawru.dbcache.dao.types.CurrencyPair;
import com.github.klawru.lib.contract.security.v1.types.request.CurrencyRateRequestDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.currency.CurrencyRateDTO;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.stereotype.Component;
import org.springframework.transaction.reactive.TransactionalOperator;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.util.List;

@Component
@RequiredArgsConstructor
public class CurrencyDBService {
    private final CurrencyPairRepository currencyPairRepository;
    private final CurrencyHistoryRepository currencyHistoryRepository;
    private final TransactionalOperator transactionalOperator;

    private static final CurrencyRateMapper currencyMapper = CurrencyRateMapper.INSTANCE;

    public Mono<Pair<CurrencyPair, List<CurrencyHistory>>> getCurrencyRate(CurrencyRateRequestDTO request) {
        return currencyPairRepository.findTopByFromAndTo(request.getCurrency().getFrom(), request.getCurrency().getTo())
                .flatMap(pair -> getHistory(pair, request.getFrom())
                        .map(list -> Pair.of(pair, list))
                );
    }

    private Mono<List<CurrencyHistory>> getHistory(CurrencyPair pair, LocalDate from) {
        final Flux<CurrencyHistory> history;
        if (from != null) {
            history = currencyHistoryRepository.findAllByPairIdAndDateAfterOrderByDateDesc(pair.getId(), from.minusDays(1));
        } else
            history = currencyHistoryRepository.findFirstByPairIdOrderByDateDesc(pair.getId());
        return history.collectList();
    }

    public Mono<CurrencyRateDTO> saveCurrencyRateAndReturn(CurrencyRateDTO currencyRateDTO) {
        final var response = Mono.just(currencyRateDTO).cache();
        return response.flatMap(pair -> currencyPairRepository.saveIgnore(currencyMapper.mapToPair(pair.getCurrency()))
                .flatMapMany(pairId -> saveHistory(pair, pairId))
                .as(transactionalOperator::transactional)
                .then(response));
    }

    private Flux<Integer> saveHistory(CurrencyRateDTO pair, Integer pairId) {
        return Mono.just(pair)
                .map(rate -> currencyMapper.mapToHistoryList(pair.getHistory(), pairId))
                .flatMapMany(currencyHistoryRepository::saveAllIgnore);
    }
}
