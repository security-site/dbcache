package com.github.klawru.dbcache.dao.mappers;


import org.eclipse.collections.impl.map.mutable.primitive.IntObjectHashMap;
import org.mapstruct.Mapper;
import org.mapstruct.Named;
import org.mapstruct.factory.Mappers;

import javax.money.Monetary;
import java.util.Currency;
import java.util.Set;

@Mapper
@Named("MoneyMapper")
public abstract class MoneyMapper {
    public static final MoneyMapper INSTANCE = Mappers.getMapper(MoneyMapper.class);
    private static final IntObjectHashMap<String> currencies = new IntObjectHashMap<>();

    static {
        Set<Currency> set = Currency.getAvailableCurrencies();
        for (Currency currency : set) {
            currencies.put(currency.getNumericCode(), currency.getCurrencyCode());
        }
    }

    @Named("currency2Integer")
    public static String currencyToCode(int currency) {
        return currencies.get(currency);
    }

    @Named("currency2Integer")
    public Integer currency2Integer(String currency) {
        if (currency == null)
            return null;
        return Monetary.getCurrency(currency).getNumericCode();
    }

}
