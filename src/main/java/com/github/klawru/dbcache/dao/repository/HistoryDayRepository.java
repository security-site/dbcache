package com.github.klawru.dbcache.dao.repository;

import com.github.klawru.dbcache.dao.types.History;
import org.apache.commons.lang3.tuple.Pair;
import org.springframework.data.r2dbc.repository.Modifying;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.r2dbc.repository.R2dbcRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.UUID;


public interface HistoryDayRepository extends R2dbcRepository<History, Pair<UUID, LocalDateTime>> {

    Flux<History> findBySecurityIdAndDateAfterOrderByDateDesc(UUID securityId, LocalDate date);

    Flux<History> findBySecurityIdOrderByDateDesc(UUID securityId);

    @Modifying
    default Flux<Integer> saveAllIgnore(Iterable<History> entity) {
        return Flux.fromIterable(entity).concatMap(history -> saveIgnore(
                history.getSecurityId(), history.getDate(),
                history.getPrice(), history.getMax(), history.getMin(), history.getOpen(), history.getClose(), history.getNumTrades(),
                history.getFaceValue(), history.getFaceValueCurrency(),
                history.getCurrency(), history.getValue()));
    }

    @Modifying
    @Query("""
            INSERT INTO history (security_id, date,
                                 price, max, min, open, close, num_trades,
                                 face_value, face_value_currency, currency, value)
            VALUES (:securityId, :date,
                    :price, :max, :min, :open, :close, :numTrades,
                    :faceValue, :faceValueCurrency, :currency, :value)
            ON CONFLICT DO NOTHING""")
    Mono<Integer> saveIgnore(UUID securityId, LocalDate date,
                             BigDecimal price, BigDecimal max, BigDecimal min, BigDecimal open, BigDecimal close, Long numTrades,
                             BigDecimal faceValue, Integer faceValueCurrency, Integer currency, BigDecimal value);
}
