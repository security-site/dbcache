package com.github.klawru.dbcache.utility;

import com.github.klawru.dbcache.AbstractApplicationIT;
import org.junit.jupiter.api.Test;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

import static org.junit.Assert.assertThrows;

class BlockHoundIT extends AbstractApplicationIT {

    @Test
    void blockHoundTest() {
        assertThrows(Exception.class, () -> Mono.fromCallable(() -> {
            Thread.sleep(1);
            return "";
        }).subscribeOn(Schedulers.parallel()).block());
    }
}
