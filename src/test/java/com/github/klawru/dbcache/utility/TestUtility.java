package com.github.klawru.dbcache.utility;

import com.github.klawru.lib.contract.security.v1.types.responce.*;
import com.github.klawru.lib.contract.security.v1.types.responce.payment.DividendDTO;
import org.javamoney.moneta.Money;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TestUtility {
    @Nonnull
    public static SecurityDTO createSecurityDTO() {
        final var fromDate = LocalDate.of(2020, 1, 1);
        final var historyDTO = createHistoryDTOList(fromDate, 1);
        return new SecurityDTO("ticker",
                "exchange",
                "isin",
                "shortName",
                "fullName",
                "shortLatName",
                "fullLatName",
                MoneyDTO.of(10, 0, "RUB"),
                1,
                fromDate,
                10L,
                Money.of(BigDecimal.TEN, "RUB"),
                new SecurityTypeDTO(SecurityType.STOCK_SHARES, SecuritySubType.COMMON_SHARE, "RAW_TYPE"),
                null,
                false,
                "site",
                historyDTO,
                List.of(new DividendDTO(fromDate, MoneyDTO.of(10, 0, "RUB"))),
                Instant.parse("2020-01-01T10:30:00.00Z")
        );
    }

    @Nonnull
    public static List<HistoryDTO> createHistoryDTOList(LocalDate fromDate, int size) {
        var date = fromDate;
        var price = 100;
        var resultList = new ArrayList<HistoryDTO>(size);
        for (int i = 0; i < size; i++) {
            date = date.plusDays(1);
            resultList.add(createHistoryDto(date, price++, "RUB"));
        }
        resultList.sort(Comparator.comparing(HistoryDTO::getDate).reversed());
        return resultList;
    }

    @Nonnull
    public static HistoryDTO createHistoryDto(LocalDate fromDate, int price, String currency) {
        return new HistoryDTO(fromDate,
                BigDecimal.valueOf(price,0),
                BigDecimal.valueOf(price,0),
                BigDecimal.valueOf(price,0),
                BigDecimal.valueOf(price,0),
                BigDecimal.valueOf(price,0),
                currency,
                MoneyDTO.of(BigDecimal.valueOf(3, 0), currency),
                BigDecimal.valueOf(1000),
                10L
        );
    }
}
