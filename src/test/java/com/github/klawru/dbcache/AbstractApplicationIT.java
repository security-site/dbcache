package com.github.klawru.dbcache;

import com.github.klawru.dbcache.config.BlockHoundConfigIntegration;
import com.github.klawru.dbcache.utility.TimescaleDBContainer;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.TestInstance;
import org.mockito.Mockito;
import org.mockito.internal.util.MockUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.context.ApplicationContext;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.PostgreSQLContainer;
import reactor.blockhound.BlockHound;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;

@Slf4j
@SpringBootTest
@ActiveProfiles("test")
@TestInstance(PER_CLASS)
public abstract class AbstractApplicationIT {
    @Autowired
    ApplicationContext applicationContext;
    @Autowired
    Set<? extends ReactiveCrudRepository<?, ?>> repositories;
    @Autowired
    CacheManager cacheManager;
    List<Object> mockedBeans;

    private static final PostgreSQLContainer<?> POSTGRES = new TimescaleDBContainer<>("timescale/timescaledb:2.5.1-pg14");

    static {
        try {
            POSTGRES.start();
            BlockHound.install(new BlockHoundConfigIntegration());
        } catch (Throwable t) {
            log.error("Failure AbstractApplicationIT static initialization", t);
            throw t;
        }

    }


    @DynamicPropertySource
    static void postgresProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.flyway.url", POSTGRES::getJdbcUrl);
        registry.add("spring.flyway.user", POSTGRES::getUsername);
        registry.add("spring.flyway.password", POSTGRES::getPassword);
        registry.add("spring.r2dbc.url", () -> "r2dbc:pool:postgres://%s:%d/%s"
                .formatted(POSTGRES.getContainerIpAddress(), POSTGRES.getFirstMappedPort(), POSTGRES.getDatabaseName()));
        registry.add("spring.r2dbc.username", POSTGRES::getUsername);
        registry.add("spring.r2dbc.password", POSTGRES::getPassword);
    }

    @AfterEach
    void tearDown() {
        clearAllRepositories();
        clearAllCaches();
        resetAllMock();
    }

    @BeforeAll
    void beforeAll() {
        //Как вариант есть библиотека MockInBean
        findAllMock();
    }

    private void clearAllRepositories() {
        repositories.forEach(ReactiveCrudRepository::deleteAll);
    }

    private void clearAllCaches() {
        cacheManager.getCacheNames()
                .forEach(cacheName -> Optional.ofNullable(cacheManager.getCache(cacheName))
                        .ifPresent(Cache::clear)
                );
    }

    private void resetAllMock() {
        mockedBeans.forEach(Mockito::reset);
    }

    private void findAllMock() {
        mockedBeans = Arrays.stream(applicationContext.getBeanDefinitionNames())
                .map(name -> applicationContext.getBean(name))
                .filter(MockUtil::isMock)
                .collect(Collectors.toList());
    }

}
