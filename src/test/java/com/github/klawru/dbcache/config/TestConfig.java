package com.github.klawru.dbcache.config;

import com.github.klawru.dbcache.service.client.ExchangeClientsService;
import org.mockito.Mockito;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.security.oauth2.jwt.ReactiveJwtDecoder;
import reactor.core.publisher.Mono;

@Configuration
public class TestConfig {

    @Bean
    ReactiveJwtDecoder jwtDecoder() {
        return s -> Mono.just(Jwt.withTokenValue(s).build());
    }

    @Bean
    @Primary
    ExchangeClientsService mockExchangeClientsService() {
        return Mockito.mock(ExchangeClientsService.class);
    }
}
