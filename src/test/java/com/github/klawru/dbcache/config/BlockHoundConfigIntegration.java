package com.github.klawru.dbcache.config;

import lombok.extern.slf4j.Slf4j;
import reactor.blockhound.BlockHound;
import reactor.blockhound.integration.BlockHoundIntegration;

@Slf4j
public class BlockHoundConfigIntegration implements BlockHoundIntegration {

    @Override
    public void applyTo(BlockHound.Builder builder) {
        // Workaround until https://github.com/reactor/reactor-core/issues/2137 is fixed
        builder.allowBlockingCallsInside("reactor.core.scheduler.BoundedElasticScheduler$BoundedState", "dispose");
        builder.allowBlockingCallsInside("reactor.core.scheduler.BoundedElasticScheduler", "schedule");

        builder.allowBlockingCallsInside("java.util.concurrent.ThreadPoolExecutor", "getTask");
        builder.allowBlockingCallsInside("org.springframework.http.codec.ResourceHttpMessageWriter", "getResourceMediaType");
        builder.allowBlockingCallsInside("java.util.UUID", "randomUUID");
        builder.allowBlockingCallsInside("java.security.SecureRandom", "nextBytes");
        builder.allowBlockingCallsInside("io.r2dbc.postgresql.authentication.SASLAuthenticationHandler", "handle");
        builder.allowBlockingCallsInside("java.util.ServiceLoader$LazyClassPathLookupIterator", "parse");
        builder.allowBlockingCallsInside("org.javamoney.moneta.spi.MonetaryConfig", "<clinit>");

        log.info("BlockHound install");
    }

}
