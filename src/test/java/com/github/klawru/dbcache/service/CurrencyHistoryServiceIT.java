package com.github.klawru.dbcache.service;

import com.github.klawru.dbcache.AbstractApplicationIT;
import com.github.klawru.dbcache.dao.repository.CurrencyHistoryRepository;
import com.github.klawru.dbcache.dao.repository.CurrencyPairRepository;
import com.github.klawru.dbcache.service.client.ExchangeClientsService;
import com.github.klawru.lib.contract.security.v1.types.request.CurrencyRateRequestDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.currency.CurrencyPairDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.currency.CurrencyRateDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.currency.RateHistoryDTO;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.recursive.comparison.RecursiveComparisonConfiguration;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.core.publisher.Mono;

import javax.annotation.Nonnull;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.util.Comparator;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static com.github.klawru.dbcache.utility.HistoryUtilsUtils.trimHistoryListFrom;
import static com.github.klawru.lib.contract.security.v1.SecurityInfo.FROM_MIN_DATE;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.util.BigDecimalComparator.BIG_DECIMAL_COMPARATOR;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@Slf4j
class CurrencyHistoryServiceIT extends AbstractApplicationIT {
    @Autowired
    CurrencyService service;
    @Autowired
    ExchangeClientsService mockClient;

    @Autowired
    CurrencyPairRepository currencyPairRepository;
    @Autowired
    CurrencyHistoryRepository currencyHistoryRepository;

    final RecursiveComparisonConfiguration configuration = RecursiveComparisonConfiguration.builder()
            .withComparatorForType(BIG_DECIMAL_COMPARATOR, BigDecimal.class)
            .build();

    @Test
    void getCurrencyRate() {
        final var fromDate = LocalDate.of(2021, 1, 11);
        final var requestDTO = CurrencyRateRequestDTO.of("R1B", "R1K", fromDate);
        final var clientResult = createCurrencyResponse(requestDTO, 100);
        Mockito.when(mockClient.getCurrencyRate(requestDTO.withFrom(FROM_MIN_DATE))).thenReturn(Mono.just(clientResult));
        Mockito.when(mockClient.getCurrencyRate(requestDTO.withFrom(null))).thenReturn(Mono.just(clientResult));
        final var expected = clientResult.withHistory(
                trimHistoryListFrom(fromDate, clientResult.getHistory(), RateHistoryDTO::getDate));
        //When
        final var actualFromClient = service.getCurrencyRate(requestDTO)
                .block();
        final var actualFromDB = service.getCurrencyRate(requestDTO)
                .block();
        assertThat(actualFromClient)
                .usingRecursiveComparison(configuration)
                .ignoringFields("history")
                .isEqualTo(actualFromDB);
        assertThat(actualFromClient.getHistory())
                .usingRecursiveFieldByFieldElementComparator(configuration)
                .isEqualTo(actualFromDB.getHistory());
        assertThat(actualFromDB)
                .usingRecursiveComparison(configuration)
                .isEqualTo(expected);
    }


    @Test
    void getCurrencyRate2() {
        final var currencyPairDTO = CurrencyPairDTO.of("BAA", "BBB");
        final var inverted = CurrencyPairDTO.of("BBB", "BAA");
        final var requestDTO = CurrencyRateRequestDTO.of(currencyPairDTO, FROM_MIN_DATE);
        final var expected = createCurrencyResponse(requestDTO, 100);
        final var rateDTOMono = Mono.just(expected);
        Mockito.when(mockClient.getCurrencyRate(requestDTO)).thenReturn(rateDTOMono);
        Mockito.when(mockClient.getCurrencyRate(requestDTO.withFrom(null))).thenReturn(rateDTOMono);
        //When
        final var actualFromClient = service.getCurrencyRate(requestDTO)
                .block();
        final var secondLoadFromDB = service.getCurrencyRate(new CurrencyRateRequestDTO(inverted, FROM_MIN_DATE))
                .block();
        //Then
        assertThat(actualFromClient)
                .usingRecursiveComparison(configuration)
                .ignoringFields("history")
                .isEqualTo(secondLoadFromDB);
        assertThat(actualFromClient.getHistory())
                .usingRecursiveFieldByFieldElementComparator(configuration)
                .isEqualTo(secondLoadFromDB.getHistory());
        assertThat(secondLoadFromDB)
                .usingRecursiveComparison(configuration)
                .isEqualTo(expected);
        verify(mockClient, times(1)).getCurrencyRate(requestDTO);
    }

    private CurrencyRateDTO createCurrencyResponse(CurrencyRateRequestDTO requestDTO, double startValue) {
        final var collect = Stream.generate(getHistoryDTOSupplier(requestDTO.getFrom(), startValue))
                .limit(20)
                .sorted(Comparator.comparing(RateHistoryDTO::getDate).reversed())
                .collect(Collectors.toList());
        return new CurrencyRateDTO(requestDTO.getCurrency(),
                collect.get(0).getRate(),
                collect
        );
    }

    @Nonnull
    private Supplier<RateHistoryDTO> getHistoryDTOSupplier(LocalDate from, double startValue) {
        return new Supplier<>() {
            double value = startValue;
            long generateFrom = from.toEpochDay();

            @Override
            public RateHistoryDTO get() {
                return new RateHistoryDTO(LocalDate.ofEpochDay(generateFrom++), BigDecimal.valueOf(value++).setScale(9, RoundingMode.HALF_EVEN));
            }
        };
    }
}