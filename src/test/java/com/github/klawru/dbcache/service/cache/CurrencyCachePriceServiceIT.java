package com.github.klawru.dbcache.service.cache;

import com.github.klawru.dbcache.AbstractApplicationIT;
import com.github.klawru.dbcache.service.client.ExchangeClientsService;
import com.github.klawru.lib.contract.security.v1.types.request.CurrencyRateRequestDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.currency.CurrencyRateDTO;
import io.rsocket.exceptions.ApplicationErrorException;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.util.concurrent.Callable;

import static org.junit.jupiter.api.Assertions.assertEquals;

@Slf4j
class CurrencyCachePriceServiceIT extends AbstractApplicationIT {
    @Autowired
    CurrencyPriceCacheService service;
    @Autowired
    ExchangeClientsService mockClient;

    @Test
    void keyTest() {
        final var rubUsd = CurrencyPriceCacheService.Key.of("RUB", "USD");
        final var usdRub = CurrencyPriceCacheService.Key.of("USD", "RUB");
        assertEquals(rubUsd, usdRub);
        assertEquals(rubUsd.hashCode(), usdRub.hashCode());
    }

    /**
     * Кеш не должен кешировать ошибки
     */
    @Test
    void getCurrencyLastPriceWhenErrorDontCache() {
        final var requestDTO = CurrencyRateRequestDTO.of("RUB", "USD", null);
        final var expected = CurrencyRateDTO.of(requestDTO.getCurrency(), BigDecimal.TEN, null);
        final var firstErrorThenOk = Mono.fromCallable(new Callable<CurrencyRateDTO>() {
                                                           boolean isFirst = true;

                                                           @Override
                                                           public CurrencyRateDTO call() throws Exception {
                                                               if (isFirst) {
                                                                   isFirst = false;
                                                                   throw new ApplicationErrorException("First subscribe error");
                                                               }
                                                               return expected;
                                                           }
                                                       }
        );
        Mockito.when(mockClient.getCurrencyRate(requestDTO)).thenReturn(firstErrorThenOk);
        //When
        //Ловим ошибку при первом вызове, и она не должна попадать в кеш
        final var rateDTOMono = service.getCurrencyLastPrice(requestDTO);
        Assertions.assertThrows(ApplicationErrorException.class, rateDTOMono::block);
        final var actual = rateDTOMono.block();
        //Then
        log.info("actual='{}'", actual);
        assertEquals(expected, actual);
    }

    /**
     * При вызове тойже инвертированной пары, возвращать из кеша результат
     */
    @Test
    void getCurrencyLastPriceWhenRequestInvertedThenOk() {
        final var rubUsdRequest = CurrencyRateRequestDTO.of("RUB", "USD", null);
        final var usdRubRequest = CurrencyRateRequestDTO.of("USD", "RUB", null);
        final var expected = CurrencyRateDTO.of(rubUsdRequest.getCurrency(), BigDecimal.TEN, null);
        Mockito.when(mockClient.getCurrencyRate(rubUsdRequest)).thenReturn(Mono.just(expected));
        //Кладем в кеш пару "RUB"-"USD"
        final var rubUsd = service.getCurrencyLastPrice(rubUsdRequest).block();
        //When
        //По ключу "USD"-"RUB" берем из кеша пару "RUB"-"USD"
        final var actual = service.getCurrencyLastPrice(usdRubRequest)
                .block();
        //Then
        log.info("actual='{}'", actual);
        assertEquals(expected, actual);
    }
}