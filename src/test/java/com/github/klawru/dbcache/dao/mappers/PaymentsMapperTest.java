package com.github.klawru.dbcache.dao.mappers;

import com.github.klawru.dbcache.dao.types.Payment;
import com.github.klawru.dbcache.dao.types.PaymentType;
import com.github.klawru.lib.contract.security.v1.types.responce.MoneyDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.payment.AmortizationDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.payment.CouponDTO;
import com.github.klawru.lib.contract.security.v1.types.responce.payment.DividendDTO;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Comparator;

import static org.assertj.core.api.Assertions.assertThat;

class PaymentsMapperTest {
    final PaymentsMapper mapper = PaymentsMapper.INSTANCE;

    @Test
    void dividend2Entity() {
        final var now = LocalDate.now();
        final var moneyDTO = MoneyDTO.of(BigDecimal.ONE, "RUB");
        final var source = new DividendDTO(now, moneyDTO);
        //when
        final var result = mapper.dtoMapToEntity(source);
        final var backMapping = mapper.entityMapToDto(result);
        //then
        assertThat(result).isNotNull()
                .usingComparatorForType(Comparator.naturalOrder(), BigDecimal.class)
                .isEqualTo(new Payment(null,
                        PaymentType.DIVIDEND,
                        now,
                        moneyDTO.getAmount(),
                        643,
                        null,
                        null,
                        null,
                        null));
    }

    @Test
    void coupon2Entity() {
        final var now = LocalDate.now();
        final var faceValue = MoneyDTO.of(BigDecimal.valueOf(1000, 0), "RUB");
        final var moneyDTO = MoneyDTO.of(BigDecimal.valueOf(100, 0), "RUB");
        final var percent = BigDecimal.valueOf(10L, 2);
        final var rubValue = MoneyDTO.of(100, 0, "RUB");
        final var source = new CouponDTO(now, moneyDTO, faceValue, percent, rubValue);

        final var expected = new Payment(null, PaymentType.COUPON, now,
                moneyDTO.getAmount(), 643,
                percent,
                rubValue.getAmount(),
                faceValue.getAmount(),
                643
        );
        //when
        final var result = mapper.dtoMapToEntity(source);
        final var backMapping = mapper.entityMapToDto(result);
        //then
        assertThat(result).isEqualTo(expected);
        assertThat(backMapping).isEqualTo(source);
    }

    @Test
    void amortization2Entity() {
        final var now = LocalDate.now();
        final var faceValue = MoneyDTO.of(BigDecimal.valueOf(1000, 0), "RUB");
        final var moneyDTO = MoneyDTO.of(BigDecimal.valueOf(100, 0), "RUB");
        final var percent = BigDecimal.valueOf(10L, 2);
        final var rubValue = MoneyDTO.of(100, 0, "RUB");
        final var source = new AmortizationDTO(now, moneyDTO, faceValue, percent, rubValue);

        final var expected = new Payment(null, PaymentType.AMORTIZATION, now,
                moneyDTO.getAmount(), 643, percent, rubValue.getAmount(),
                faceValue.getAmount(), 643
        );
        //when
        final var result = mapper.dtoMapToEntity(source);
        final var backMapped = mapper.entityMapToDto(result);
        //then
        assertThat(result).isEqualTo(expected);
        assertThat(backMapped).isEqualTo(source);
    }
}