package com.github.klawru.dbcache.dao.repository;

import com.github.klawru.dbcache.AbstractApplicationIT;
import com.github.klawru.dbcache.dao.types.CurrencyPair;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

class CurrencyPairRepositoryIT extends AbstractApplicationIT {

    @Autowired
    CurrencyPairRepository repository;

    @Test
    void saveIgnoreDuplicatesShouldEqualId() {
        final var id = repository.saveIgnore(new CurrencyPair(null, "RUB", "EUR"))
                .block();
        final var id2 = repository.saveIgnore(new CurrencyPair(null, "RUB", "EUR"))
                .block();
        assertEquals(id, id2);
    }

    @Test
    void saveIgnoreShouldDifferentIds() {
        final var id = repository.saveIgnore(new CurrencyPair(null, "RAB", "RAC"))
                .block();
        final var id2 = repository.saveIgnore(new CurrencyPair(null, "RAB", "USD"))
                .block();
        assertNotEquals(id, id2);
    }
}
