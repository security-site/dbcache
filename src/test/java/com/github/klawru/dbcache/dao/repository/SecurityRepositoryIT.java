package com.github.klawru.dbcache.dao.repository;

import com.github.klawru.dbcache.AbstractApplicationIT;
import com.github.klawru.dbcache.dao.mappers.SecurityMapper;
import com.github.klawru.dbcache.utility.TestUtility;
import com.github.klawru.lib.contract.security.v1.types.responce.SecurityDTO;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.Instant;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SecurityRepositoryIT extends AbstractApplicationIT {
    @Autowired
    SecurityRepository repository;

    @Test
    void saveIgnoreShouldSameId() {
        final SecurityDTO security = TestUtility.createSecurityDTO();
        final var entity = SecurityMapper.INSTANCE.dtoToEntity(security).withId(UUID.fromString("123e4567-e89b-12d3-a456-426655440000"));
        entity.setFullUpdate(Instant.parse("2020-01-01T10:30:00.00Z"));
        final var save = repository.saveIgnore(entity).block();
        final var same = repository.saveIgnore(entity.withId(UUID.randomUUID())).block();
        assertEquals(same.getId(), save.getId());
    }


}